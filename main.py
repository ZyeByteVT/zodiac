#!/usr/bin/env python3

from typing import *
from datetime import datetime, timedelta
from config import *

import importlib
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Zodiac',
        description='A simple schedule renderer',
    )

    parser.add_argument('-s', '--start', type=str, help='Start date in ISO format')
    parser.add_argument('-d', '--days', type=int, help='The amount of days to render')
    parser.add_argument('-c', '--config', type=str, help='Configuration file path')
    parser.add_argument('-o', '--output', type=str, help='Output file path')

    args = parser.parse_args()

    day_count = args.days if args.days else 7
    start_date = datetime.fromisoformat(args.start) if args.start else datetime.now()
    end_date = start_date + timedelta(days=day_count)
    config_path = args.config if args.config else 'config.json'
    output_path = args.output if args.output else 'output.png'

    config = parse_config(config_path, start_date, end_date)
    r = config['renderer_type'](
        entries=config['entries'],
        start_date=start_date,
        day_count=day_count,
        options=config['renderer_options']
    )

    r.draw()
    r.save(output_path)
    