from PIL import ImageDraw, Image, ImageFont
from config import *
from typing import *
from ptypes import *
from abc import abstractmethod, ABC
from datetime import date

def get_entry_for_date(entries: list[ScheduleEntry], date: date) -> Optional[ScheduleEntry]:
    return next((e for e in entries if e['datetime'].date() == date), None)


class Renderer(ABC):
    _base_image: Image
    _drawer: ImageDraw

    def initialize(self, size: Point):
        self._base_image = Image.new('RGB', size, (255, 255, 255, 255))
        self._drawer = ImageDraw.Draw(self._base_image)


    def draw_image(self, image: Image, position: Point) -> None:
        self._base_image.paste(image, position)


    def draw_text(self, text: str, position: Point, **kwargs) -> None:
        self._drawer.text(position, text, **kwargs)


    @abstractmethod
    def draw(self) -> None:
        ...

    
    def save(self, path: str) -> None:
        self._base_image.save(path)