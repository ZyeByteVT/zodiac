from renderer import *
from ptypes import *
from datetime import datetime, timedelta

class ZyeByteRenderer(Renderer):
    DAY_NAMES = ('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')
    FONT_PATH = 'aldrich.regular.ttf'

    def __init__(self, entries: list[ScheduleEntry], start_date: datetime, day_count: int, options: dict[str, Any] = {}):
        assert day_count == 7, "ZyeByteRenderer requires exactly 7 days"

        self.start_date = start_date
        self.end_date = start_date + timedelta(days=day_count)
        self.day_count = day_count
        self.entries = entries
        self.options = options

        self.fonts = {
            'day': ImageFont.truetype(self.FONT_PATH, 37),
            'title': ImageFont.truetype(self.FONT_PATH, 40),
            'desc': ImageFont.truetype(self.FONT_PATH, 23),
            'time': ImageFont.truetype(self.FONT_PATH, 20),
        }


    def draw_day_free(self, position: Point, weekday: int) -> None:
        self.draw_text(self.DAY_NAMES[weekday], (position[0] + 66, position[1] + 47), font=self.fonts['day'], anchor='mm', fill='#ffffff')
        self.draw_text('Recharging...', (position[0] + 428, position[1] + 47), font=self.fonts['title'], anchor='mm', fill='#a5d6ea')
    

    def draw_day_schedule(self, position: Point, weekday: int, entry: ScheduleEntry) -> None:
        self.draw_text(self.DAY_NAMES[weekday], (position[0] + 66, position[1] + 40), font=self.fonts['day'], anchor='mm', fill='#ffffff')
        self.draw_text(entry['datetime'].strftime('%H:%M'), (position[0] + 66, position[1] + 67), font=self.fonts['time'], anchor='mm', fill='#ffffff')
        self.draw_text(entry['title'], (position[0] + 428, position[1] + 35), font=self.fonts['title'], anchor='mm', fill='#125f99')
        self.draw_text(entry['description'], (position[0] + 428, position[1] + 67), font=self.fonts['desc'], anchor='mm', fill='#125f99')
    

    def draw(self) -> None:
        self.initialize((1920, 1080))

        current_date = self.start_date.date()
        
        with Image.open('base.png') as base:
            self.draw_image(base, (0, 0))

        self.draw_text(self.start_date.strftime('%b %d') + ' - ' + self.end_date.strftime('%b %d'),
            (130, 206), font=self.fonts['day'], fill='#ffffff')

        for i in range(self.day_count):
            weekday = current_date.weekday()
            entry = get_entry_for_date(self.entries, current_date)
            current_date += timedelta(days=1)

            base_y = 296 + 110 * i

            if entry is None:
                self.draw_day_free((102, base_y), weekday)
            else:
                self.draw_day_schedule((102, base_y), weekday, entry)
        
