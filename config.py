from typing import *
from datetime import datetime
from renderer import *
from ptypes import *
import json
import inspect
import importlib

def parse_config(path: str, start_date: datetime, end_date: datetime) -> dict:
    with open(path, 'r') as file:
        config = json.load(file)

    renderer_fqn = config['renderer']['class'].rsplit('.', 1)
    if len(renderer_fqn) != 2:
        raise Exception("Renderer must be in the format 'module.Class'")

    renderer_type = getattr(importlib.import_module(renderer_fqn[0]), renderer_fqn[1])
    if not inspect.isclass(renderer_type) or not issubclass(renderer_type, Renderer):
        raise Exception(f"Invalid renderer type {config['renderer']}")

    entries: list[ScheduleEntry] = []

    for entry in config['entries']:
        entry['datetime'] = datetime.fromisoformat(entry['datetime'])

        if start_date <= entry['datetime'] <= end_date:
            entries.append(ScheduleEntry(**entry))

    return {
        'renderer_type': renderer_type,
        'renderer_options': config['renderer']['options'],
        'entries': entries,
    }