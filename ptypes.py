from datetime import datetime
from typing import *

Point = tuple[int, int]

ColorRGBA = tuple[int, int, int, int]
Color = str | ColorRGBA

ScheduleEntry = TypedDict('ScheduleEntry', {
    'at': datetime,
    'title': str,
    'description': str
})

ConfigData = TypedDict('ConfigData', {
    'renderer': type,
    'entries': list[ScheduleEntry]
})